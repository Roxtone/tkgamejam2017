﻿using UnityEngine.Events;

[System.Serializable]
public class PlayerEvent : UnityEvent<Player>
{
}