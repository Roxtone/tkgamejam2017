﻿using UnityEngine.Events;

[System.Serializable]
public class MeteoriteEvent : UnityEvent<Meteorite>
{
}