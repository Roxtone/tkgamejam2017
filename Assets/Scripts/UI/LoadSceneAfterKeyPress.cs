﻿using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;

public class LoadSceneAfterKeyPress : MonoBehaviour
{
    [SerializeField]
    private string sceneName;

    private bool isLoading;

    void Update()
    {
        if (!isLoading)
        {
            HandleKeyPress();
        }
    }

    private void HandleKeyPress()
    {
        if (Input.anyKeyDown)
        {
            StartCoroutine(LoadSceneCoroutine());
        }
    }

    private IEnumerator LoadSceneCoroutine()
    {
        isLoading = true;
        Fade.Instance.FadeOut();
        yield return new WaitForSeconds(Fade.Instance.FadeDuration);
        SceneManager.LoadSceneAsync(sceneName);
    }
}