﻿using UnityEngine;
using UnityEngine.UI;

public class WinScreen : MonoBehaviour
{
    [SerializeField]
    private Text playerWonText;

    void Awake()
    {
        gameObject.SetActive(false);
        EventManager.Instance.PlayerWon.AddListener(OnPlayerWon);
    }

    private void OnPlayerWon(Player player)
    {
        playerWonText.text = string.Format(playerWonText.text, player.Name);
        gameObject.SetActive(true);
    }
}