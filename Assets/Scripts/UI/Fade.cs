﻿using UnityEngine;

public class Fade : Singleton<Fade>
{
    [SerializeField]
    private float fadeSpeed;
    [SerializeField]
    private Texture2D texture;

    private float fadeDirection;
    private float alpha;

    void Awake()
    {
        alpha = 1.0f;
        FadeIn();
    }

    void OnGUI()
    {
        UpdateFade();
        DrawFade();
    }

    public float FadeDuration { get { return 1.0f / fadeSpeed; } }

    public void FadeOut()
    {
        fadeDirection = 1.0f;
    }

    public void FadeIn()
    {
        fadeDirection = -1.0f;
    }

    private void UpdateFade()
    {
        alpha += fadeDirection * fadeSpeed * Time.deltaTime;
        alpha = Mathf.Clamp(alpha, 0.0f, 1.0f);
    }

    private void DrawFade()
    {
        Color color = GUI.color;
        color.a = alpha;
        GUI.color = color;
        GUI.DrawTexture(new Rect(0, 0, Screen.width, Screen.height), texture);
    }
}