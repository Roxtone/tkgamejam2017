﻿using System.IO;
using UnityEditor;
using UnityEngine;

public class PaletteTextureGenerator : EditorWindow
{
    private const int ColumnCount = 4;
    private const int RowCount = 2;
    private const int CellSize = 256;
    private const string folderName = "Textures/Palettes";
    private const string pathSeparator = "/";
    private const string extension = ".png";

    private string fileName = "Palette";
    private Color[] colors;

    [MenuItem("Texture Palette/Generate Texture")]
    public static void Initialize()
    {
        PaletteTextureGenerator window = GetWindow<PaletteTextureGenerator>();
        window.Setup();
        window.Show();
    }

    public void Setup()
    {
        colors = new Color[ColumnCount * RowCount];
        for (int i = 0; i < colors.Length; i++)
        {
            colors[i] = Random.ColorHSV();
        }
    }

    void OnGUI()
    {
        DrawColorPickers();
        DrawNameInput();
        DrawGenerateButton();
    }

    private void DrawColorPickers()
    {
        for (int i = 0; i < colors.Length; i++)
        {
            colors[i] = EditorGUILayout.ColorField("Color " + i, colors[i]);
        }
    }

    private void DrawNameInput()
    {
        fileName = EditorGUILayout.TextField("Name", fileName);
    }

    private void DrawGenerateButton()
    {
        if (GUILayout.Button("Generate"))
        {
            GenerateTexture();
            Close();
        }
    }

    private void GenerateTexture()
    {
        Texture2D texture = new Texture2D(ColumnCount * CellSize, RowCount * CellSize, TextureFormat.ARGB32, false);
        for (int x = 0; x < texture.width; x++)
        {
            for (int y = 0; y < texture.height; y++)
            {
                int colorIndex = (x / CellSize) + (y / CellSize) * ColumnCount;
                texture.SetPixel(x, CellSize * RowCount - y, colors[colorIndex]);
            }
        }
        texture.Apply();
        SaveTexture(texture);
        DestroyImmediate(texture);
    }

    private void SaveTexture(Texture2D texture)
    {
        byte[] bytes = texture.EncodeToPNG();
        string path = Application.dataPath + pathSeparator + folderName + pathSeparator + fileName + extension;
        File.WriteAllBytes(path, bytes);
        AssetDatabase.Refresh();
    }
}