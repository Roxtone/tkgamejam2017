﻿using UnityEngine;

public class ScreenshakeHandler : MonoBehaviour
{
    void Awake()
    {
        EventManager.Instance.MeteoriteFell.AddListener(OnMeteoriteFell);
        EventManager.Instance.MeteoriteThrown.AddListener(OnMeteoriteThrown);
        EventManager.Instance.MeteoriteCollided.AddListener(OnMeteoriteCollided);
        EventManager.Instance.MeteoriteHit.AddListener(OnMeteoriteHit);
    }

    private void OnMeteoriteFell(Meteorite meteorite)
    {
        CameraManager.Instance.AddScreenshake();
    }

    private void OnMeteoriteThrown(Meteorite meteorite)
    {
        CameraManager.Instance.AddScreenshake();
    }

    private void OnMeteoriteCollided(Meteorite meteorite)
    {
        CameraManager.Instance.AddScreenshake();
    }

    private void OnMeteoriteHit(Meteorite meteorite)
    {
        CameraManager.Instance.AddScreenshake();
    }
}