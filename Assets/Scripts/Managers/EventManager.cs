﻿public class EventManager : Singleton<EventManager>
{
    public PlayerEvent PlayerDied;
    public PlayerEvent PlayerWon;
    public MeteoriteEvent MeteoriteFell;
    public MeteoriteEvent MeteoriteThrown;
    public MeteoriteEvent MeteoriteCollided;
    public MeteoriteEvent MeteoriteHit;
}