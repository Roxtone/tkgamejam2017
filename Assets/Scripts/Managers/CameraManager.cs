﻿using UnityEngine;

public class CameraManager : Singleton<CameraManager>
{
    [SerializeField]
    private float screenshakeDecay;
    [SerializeField]
    private float defaultScreenshakeAmount;
    [SerializeField]
    private float directorSpeed;
    [SerializeField]
    private float zoomMarginMin;
    [SerializeField]
    private float zoomMarginMax;
    [SerializeField]
    private float minZoom;
    [SerializeField]
    private float maxZoom;

    private Camera directorCamera;
    private Vector3 directorPosition;
    private float currentZoom;
    private float currentScreenshakeAmount;

    void Awake()
    {
        directorCamera = GetComponent<Camera>();
        directorPosition = transform.position;
    }

    void LateUpdate()
    {
        UpdateScreenshake();
        UpdateDirector();
        UpdateCamera();
    }

    public void AddScreenshake()
    {
        AddScreenshake(defaultScreenshakeAmount);
    }

    public void AddScreenshake(float amount)
    {
        currentScreenshakeAmount += amount;
    }

    private void UpdateScreenshake()
    {
        currentScreenshakeAmount = Mathf.Max(currentScreenshakeAmount - screenshakeDecay * Time.deltaTime, 0.0f);
    }

    private void UpdateDirector()
    {
        UpdateDirectorCenter();
        UpdateDirectorZoom();
    }

    private void UpdateDirectorCenter()
    {
        Player[] players = GameFlowManager.Instance.Players;
        Vector3 center = Vector3.zero;
        for (int i = 0; i < players.Length; i++)
        {
            if (!players[i].IsDead)
            {
                center += players[i].transform.position;
            }
        }
        center = center / players.Length;
        directorPosition.x = Mathf.Lerp(directorPosition.x, center.x, directorSpeed * Time.deltaTime);
    }

    private void UpdateDirectorZoom()
    {
        Player[] players = GameFlowManager.Instance.Players;
        float zoomFactor = 0.0f;
        for (int i = 0; i < players.Length && zoomFactor >= 0.0f; i++)
        {
            Vector3 screenPosition = directorCamera.WorldToViewportPoint(players[i].transform.position);
            if (screenPosition.x > zoomMarginMax && screenPosition.x < 1.0f - zoomMarginMax && screenPosition.y > zoomMarginMax && screenPosition.y < 1.0f - zoomMarginMax)
            {
                zoomFactor = 1.0f;
            }
            else if (screenPosition.x < zoomMarginMin || screenPosition.x > 1.0f - zoomMarginMin || screenPosition.y < zoomMarginMin || screenPosition.y > 1.0f - zoomMarginMin)
            {
                zoomFactor = -1.0f;
            }
        }
        float previousZoom = currentZoom;
        currentZoom = Mathf.Clamp(currentZoom + zoomFactor * directorSpeed * Time.deltaTime, minZoom, maxZoom);
        directorPosition += transform.forward * (currentZoom - previousZoom);
    }

    private void UpdateCamera()
    {
        Vector3 finalPosition = directorPosition + Random.insideUnitSphere * currentScreenshakeAmount;
        transform.position = finalPosition;
    }
}