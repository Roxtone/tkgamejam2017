﻿using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class GameFlowManager : Singleton<GameFlowManager>
{
    private Player winner;

    void Start()
    {
        Players = FindObjectsOfType<Player>();
        EventManager.Instance.PlayerDied.AddListener(OnPlayerDied);
    }

    public Player[] Players { get; private set; }

    private void CheckForWinner()
    {
        IEnumerable<Player> alivePlayers = Players.Where(player => !player.IsDead);
        if (alivePlayers.Count() == 1)
        {
            winner = alivePlayers.First();
            EventManager.Instance.PlayerWon.Invoke(winner);
        }
    }

    private void OnPlayerDied(Player player)
    {
        if (winner == null)
        {
            CheckForWinner();
        }
    }
}