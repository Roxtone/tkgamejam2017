﻿using UnityEngine;

public abstract class MeteoriteBehaviour : MonoBehaviour
{
    private Meteorite meteorite;
    private Rigidbody rigidBody;

    protected Meteorite Meteorite
    {
        get
        {
            if (meteorite == null)
            {
                meteorite = GetComponentInParent<Meteorite>();
            }
            return meteorite;
        }
    }

    protected Rigidbody RigidBody
    {
        get
        {
            if (rigidBody == null)
            {
                rigidBody = GetComponentInParent<Rigidbody>();
            }
            return rigidBody;
        }
    }
}