﻿using System.Collections;
using UnityEngine;

public class MeteoriteSpawner : MonoBehaviour
{
    [SerializeField]
    private Meteorite meteoritePrefab;
    [SerializeField]
    private float spawnInterval;
    [SerializeField]
    private float spawnIntervalVariance;
    [SerializeField]
    private Bounds spawnBox;

    void Awake()
    {
        StartCoroutine(SpawnCoroutine());
    }

    private IEnumerator SpawnCoroutine()
    {
        while (true)
        {
            float timeToSpawn = spawnInterval + Random.Range(-spawnIntervalVariance, spawnIntervalVariance);
            yield return new WaitForSeconds(timeToSpawn);
            Spawn();
        }
    }

    private void Spawn()
    {
        Vector3 spawnPosition = CalculateSpawnPosition();
        Instantiate(meteoritePrefab, spawnPosition, Quaternion.identity, transform);
    }

    private Vector3 CalculateSpawnPosition()
    {
        Vector3 center = spawnBox.center;
        Vector3 offset = new Vector3(Random.Range(-spawnBox.extents.x, spawnBox.extents.x), 0.0f, Random.Range(-spawnBox.extents.z, spawnBox.extents.z));
        return center + offset;
    }
}