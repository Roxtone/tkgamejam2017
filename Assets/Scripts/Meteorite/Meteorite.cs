﻿using UnityEngine;

public class Meteorite : MonoBehaviour
{
    public Player Owner { get; set; }
    public bool HasFallen { get; set; }
    public bool IsThrown { get; set; }
}