﻿using UnityEngine;

public class MeteoriteParticleHandler : MonoBehaviour
{
    [SerializeField]
    private ParticleSystem impactParticlesPrefab;

    void Awake()
    {
        EventManager.Instance.MeteoriteFell.AddListener(OnMeteoriteFell);
        EventManager.Instance.MeteoriteCollided.AddListener(OnMeteoriteCollided);
    }

    private void SpawnImpactParticles(Meteorite meteorite)
    {
        Instantiate(impactParticlesPrefab, meteorite.transform.position, Quaternion.identity);
    }

    private void OnMeteoriteFell(Meteorite meteorite)
    {
        SpawnImpactParticles(meteorite);
    }

    private void OnMeteoriteCollided(Meteorite meteorite)
    {
        SpawnImpactParticles(meteorite);
    }
}