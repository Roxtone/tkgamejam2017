﻿using System.Collections;
using UnityEngine;

public class MeteoriteAttack : MeteoriteBehaviour
{
    [SerializeField]
    private float attackForce;
    [SerializeField]
    private float decayTime;

    void OnTriggerEnter(Collider other)
    {
        Meteorite otherMeteorite = other.GetComponent<Meteorite>();
        if (otherMeteorite != null && otherMeteorite.Owner != null && otherMeteorite.Owner != Meteorite.Owner && gameObject.GetInstanceID() > other.gameObject.GetInstanceID())
        {
            Collide(otherMeteorite);
        }
    }

    public void Attack()
    {
        transform.parent = null;
        RigidBody.isKinematic = false;
        RigidBody.useGravity = false;
        Vector3 force = transform.forward * attackForce;
        RigidBody.AddForce(force, ForceMode.Impulse);
        Meteorite.IsThrown = true;
        StartCoroutine(DecayCoroutine());
    }

    private IEnumerator DecayCoroutine()
    {
        yield return new WaitForSeconds(decayTime);
        Destroy(gameObject);
    }

    private void Collide(Meteorite otherMeteorite)
    {
        EventManager.Instance.MeteoriteCollided.Invoke(Meteorite);
        EventManager.Instance.MeteoriteCollided.Invoke(otherMeteorite);
        Destroy(gameObject);
        Destroy(otherMeteorite.gameObject);
    }
}