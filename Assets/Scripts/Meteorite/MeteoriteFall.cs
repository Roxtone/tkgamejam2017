﻿using System.Collections;
using UnityEngine;

public class MeteoriteFall : MeteoriteBehaviour
{
    [SerializeField]
    private Vector3 fallVelocity;
    [SerializeField]
    private float decayTime;

    void Awake()
    {
        Fall();
    }

    void OnTriggerEnter(Collider other)
    {
        if (!Meteorite.HasFallen && (other.GetComponent<Floor>() != null || other.GetComponent<PlayerMeteoriteRing>() != null))
        {
            EndFall();
        }
    }

    private void Fall()
    {
        RigidBody.velocity = fallVelocity;
        StartCoroutine(DecayCoroutine());
    }

    private void EndFall()
    {
        RigidBody.isKinematic = true;
        Meteorite.HasFallen = true;
        EventManager.Instance.MeteoriteFell.Invoke(Meteorite);
    }

    private IEnumerator DecayCoroutine()
    {
        yield return new WaitForSeconds(decayTime);
        if (Meteorite.Owner == null)
        {
            Destroy(gameObject);
        }
    }
}