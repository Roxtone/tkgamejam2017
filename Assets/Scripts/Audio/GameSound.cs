﻿using UnityEngine;

public class GameSound : MonoBehaviour
{
    [SerializeField]
    private AudioClip meteoriteImpact;
    [SerializeField]
    private AudioClip meteoriteThrow;
    [SerializeField]
    private AudioClip scream;

    private AudioSource audioSource;

    void Awake()
    {
        audioSource = GetComponent<AudioSource>();
        EventManager.Instance.MeteoriteFell.AddListener(OnMeteoriteFell);
        EventManager.Instance.MeteoriteThrown.AddListener(OnMeteoriteThrown);
        EventManager.Instance.MeteoriteCollided.AddListener(OnMeteoriteCollided);
        EventManager.Instance.MeteoriteHit.AddListener(OnMeteoriteHit);
        EventManager.Instance.PlayerDied.AddListener(OnPlayerDied);
    }

    private void OnMeteoriteFell(Meteorite meteorite)
    {
        audioSource.PlayOneShot(meteoriteImpact);
    }

    private void OnMeteoriteThrown(Meteorite meteorite)
    {
        audioSource.PlayOneShot(meteoriteThrow);
    }

    private void OnMeteoriteCollided(Meteorite meteorite)
    {
        audioSource.PlayOneShot(meteoriteImpact);
    }

    private void OnMeteoriteHit(Meteorite meteorite)
    {
        audioSource.PlayOneShot(meteoriteImpact);
    }

    private void OnPlayerDied(Player player)
    {
        audioSource.PlayOneShot(scream);
    }
}