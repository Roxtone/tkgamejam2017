﻿using UnityEngine;

public class DestroyAfter : MonoBehaviour
{
    [SerializeField]
    private float time;

    void Awake()
    {
        Destroy(gameObject, time);
    }
}