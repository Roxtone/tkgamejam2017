﻿using UnityEngine;

public class PlayerDirection : PlayerBehaviour
{
    [SerializeField]
    private Transform playerModel;
    [SerializeField]
    private float deadzone;
    [SerializeField]
    private float directionChangeSpeed;

    void Awake()
    {
        ChooseStartDirection();
    }

    void Update()
    {
        if (!Player.IsDead)
        {
            HandleDirection();
        }
    }

    private void ChooseStartDirection()
    {
        Vector3 startDirection = Vector3.zero - playerModel.position;
        startDirection.y = 0.0f;
        ApplyDirection(startDirection, false);
    }

    private void HandleDirection()
    {
        if (Mathf.Abs(PlayerInput.DirectionAxisX) > deadzone || Mathf.Abs(PlayerInput.DirectionAxisY) > deadzone)
        {
            Vector3 direction = new Vector3(PlayerInput.DirectionAxisX, 0.0f, PlayerInput.DirectionAxisY);
            ApplyDirection(direction, true);
        }
    }

    private void ApplyDirection(Vector3 direction, bool smooth)
    {
        if (smooth)
        {
            direction = Vector3.Lerp(playerModel.forward, direction, directionChangeSpeed * Time.deltaTime);
        }
        playerModel.LookAt(playerModel.position + direction);
    }
}