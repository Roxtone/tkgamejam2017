﻿using UnityEngine;

public abstract class PlayerBehaviour : MonoBehaviour
{
    private IPlayerInput playerInput;
    private Player player;
    private Rigidbody rigidBody;

    protected IPlayerInput PlayerInput
    {
        get
        {
            if (playerInput == null)
            {
                playerInput = GetComponentInParent<IPlayerInput>();
            }
            return playerInput;
        }
    }

    protected Player Player
    {
        get
        {
            if (player == null)
            {
                player = GetComponentInParent<Player>();
            }
            return player;
        }
    }

    protected Rigidbody RigidBody
    {
        get
        {
            if (rigidBody == null)
            {
                rigidBody = GetComponentInParent<Rigidbody>();
            }
            return rigidBody;
        }
    }
}