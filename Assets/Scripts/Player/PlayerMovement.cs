﻿using UnityEngine;

public class PlayerMovement : PlayerBehaviour
{
    [SerializeField]
    private float movementSpeed;

    void Update()
    {
        if (!Player.IsDead)
        {
            HandleMovement();
        }
    }

    private void HandleMovement()
    {
        Vector3 movementVector = new Vector3(PlayerInput.MovementAxisX, 0.0f, PlayerInput.MovementAxisY);
        Vector3 velocity = movementVector.normalized * movementSpeed;
        RigidBody.velocity = velocity;
    }
}