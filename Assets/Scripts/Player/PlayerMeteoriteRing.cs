﻿using System;
using UnityEngine;

public class PlayerMeteoriteRing : PlayerBehaviour
{
    [SerializeField]
    private float radius;
    [SerializeField]
    private int slotCount;
    [SerializeField]
    private float rotationSpeed;
    [SerializeField]
    private Transform playerModel;
    [SerializeField]
    private float attackInvertal;

    private Meteorite[] slots;
    private int attachedCount;
    private bool isManualMode;
    private float attackTimer;

    void Awake()
    {
        slots = new Meteorite[slotCount];
        attachedCount = 0;
        EventManager.Instance.MeteoriteCollided.AddListener(OnMeteoriteCollided);
    }

    void Update()
    {
        if (!Player.IsDead)
        {
            HandleManualModeToggle();
            HandleRotation();
            HandleAttack();
            UpdateAttackTimer();
        }
    }

    void OnTriggerEnter(Collider other)
    {
        Meteorite meteorite = other.GetComponent<Meteorite>();
        if (meteorite != null && meteorite.Owner == null && meteorite.Owner != Player && !Player.IsDead)
        {
            Attach(meteorite);
        }
    }

    private void HandleManualModeToggle()
    {
        if (PlayerInput.UseAxis > 0.0f)
        {
            isManualMode = !isManualMode;
        }
    }

    private void HandleRotation()
    {
        float rotation;
        if (isManualMode)
        {
            rotation = PlayerInput.RotationAxisRight - PlayerInput.RotationAxisLeft;
        }
        else
        {
            rotation = 1.0f;
        }
        transform.Rotate(Vector3.up, rotation * rotationSpeed * Time.deltaTime);
    }

    private void HandleAttack()
    {
        if (PlayerInput.AttackAxis > 0.0f && attackTimer < 0.0f)
        {
            Vector3 direction = playerModel.forward;
            int slotIndex = CalculateClosestSlotIndex(transform.position + direction * radius, false);
            Attack(slotIndex);
        }
    }

    private void UpdateAttackTimer()
    {
        attackTimer -= Time.deltaTime;
    }

    private void Attack(int slotIndex)
    {
        Meteorite meteorite = slots[slotIndex];
        if (meteorite != null)
        {
            meteorite.GetComponent<MeteoriteAttack>().Attack();
            slots[slotIndex] = null;
            attachedCount--;
            attackTimer = attackInvertal;
            EventManager.Instance.MeteoriteThrown.Invoke(meteorite);
        }
    }

    private void Attach(Meteorite meteorite)
    {
        if (attachedCount < slotCount)
        {
            int slotIndex = CalculateClosestSlotIndex(meteorite.transform.position, true);
            AttachToSlot(meteorite, slotIndex);
            attachedCount++;
        }
    }

    private void AttachToSlot(Meteorite meteorite, int slotIndex)
    {
        slots[slotIndex] = meteorite;
        meteorite.Owner = Player;
        meteorite.transform.parent = transform;
        meteorite.transform.localPosition = CalculateSlotLocalPosition(slotIndex);
        Vector3 fromCenter = meteorite.transform.position - transform.position;
        meteorite.transform.LookAt(meteorite.transform.position + fromCenter);
    }

    private int CalculateClosestSlotIndex(Vector3 position, bool empty)
    {
        int closestIndex = -1;
        float closestDistance = float.MaxValue;
        for (int i = 0; i < slots.Length; i++)
        {
            if (!empty || slots[i] == null)
            {
                float distance = Vector3.Distance(position, CalculateSlotGlobalPosition(i));
                if (distance < closestDistance)
                {
                    closestIndex = i;
                    closestDistance = distance;
                }
            }
        }
        return closestIndex;
    }

    private Vector3 CalculateSlotLocalPosition(int slotIndex)
    {
        float arc = 360.0f * slotIndex / slotCount;
        Quaternion rotation = Quaternion.AngleAxis(arc, Vector3.up);
        Vector3 position = rotation * Vector3.forward * radius;
        return position;
    }

    private Vector3 CalculateSlotGlobalPosition(int slotIndex)
    {
        return transform.TransformPoint(CalculateSlotLocalPosition(slotIndex));
    }

    private void OnMeteoriteCollided(Meteorite meteorite)
    {
        int slotIndex = Array.IndexOf(slots, meteorite);
        if (slotIndex > -1)
        {
            slots[slotIndex] = null;
            attachedCount--;
        }
    }
}