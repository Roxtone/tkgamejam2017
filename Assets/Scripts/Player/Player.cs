﻿using UnityEngine;

public class Player : MonoBehaviour
{
    public bool IsDead { get; set; }
    public string Name { get; set; }
}