﻿using UnityEngine;

public class PlayerMovementAnimation : PlayerBehaviour
{
    [SerializeField]
    private float leanAngle;

    void Update()
    {
        AnimateMovement();
    }

    private void AnimateMovement()
    {
        Vector3 velocity = RigidBody.velocity;
        if (velocity.magnitude > 0.0f)
        {
            Vector3 axis = Vector3.Cross(Vector3.up, transform.InverseTransformDirection(velocity.normalized));
            transform.localRotation = Quaternion.AngleAxis(leanAngle, axis);
        }
        else
        {
            transform.localRotation = Quaternion.identity;
        }
    }
}