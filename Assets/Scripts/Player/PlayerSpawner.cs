﻿using UnityEngine;

public class PlayerSpawner : MonoBehaviour
{
    [SerializeField]
    private Player playerPrefab;
    [SerializeField]
    private Vector3 spawnPosition;
    [SerializeField]
    private string playerName;

    void Awake()
    {
        Spawn();
    }

    private void Spawn()
    {
        Player player = Instantiate(playerPrefab, spawnPosition, Quaternion.identity, transform);
        player.Name = playerName;
    }
}