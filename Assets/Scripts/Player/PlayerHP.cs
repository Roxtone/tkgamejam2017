﻿using UnityEngine;

public class PlayerHP : PlayerBehaviour
{
    [SerializeField]
    private int startHP;
    [SerializeField]
    private float deathForce;
    [SerializeField]
    private float deathTorque;

    void Awake()
    {
        CurrentHP = startHP;
    }

    void OnTriggerEnter(Collider other)
    {
        Meteorite meteorite = other.GetComponent<Meteorite>();
        if (meteorite != null && meteorite.Owner != null && meteorite.Owner != Player && meteorite.IsThrown && !Player.IsDead)
        {
            TakeDamage(meteorite);
        }
    }

    public int CurrentHP { get; private set; }

    private void TakeDamage(Meteorite meteorite)
    {
        EventManager.Instance.MeteoriteCollided.Invoke(meteorite);
        CurrentHP = Mathf.Max(CurrentHP - 1, 0);
        if (CurrentHP == 0)
        {
            Die(meteorite);
        }
        Destroy(meteorite.gameObject);
    }

    private void Die(Meteorite meteorite)
    {
        Player.IsDead = true;
        RigidBody.constraints = RigidbodyConstraints.None;
        RigidBody.AddForce(Vector3.up * deathForce, ForceMode.Impulse);
        RigidBody.AddTorque(Random.insideUnitSphere * deathTorque);
        EventManager.Instance.PlayerDied.Invoke(Player);
    }
}