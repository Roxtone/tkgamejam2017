﻿using UnityEngine;

public class PlayerInputDebug : MonoBehaviour
{
    private IPlayerInput playerInput;

    void Awake()
    {
        playerInput = GetComponent<IPlayerInput>();
    }

    void OnGUI()
    {
        GUILayout.Label("AttackAxis: " + playerInput.AttackAxis);
        GUILayout.Label("DirectionAxisX: " + playerInput.DirectionAxisX);
        GUILayout.Label("DirectionAxisY: " + playerInput.DirectionAxisY);
        GUILayout.Label("MovementAxisX: " + playerInput.MovementAxisX);
        GUILayout.Label("MovementAxisY: " + playerInput.MovementAxisY);
        GUILayout.Label("RotationAxisLeft: " + playerInput.RotationAxisLeft);
        GUILayout.Label("RotationAxisRight: " + playerInput.RotationAxisRight);
        GUILayout.Label("UseAxis: " + playerInput.UseAxis);
    }
}