﻿using UnityEngine;

public class ControllerPlayerInput : MonoBehaviour, IPlayerInput
{
    [SerializeField]
    private string attackAxis;
    [SerializeField]
    private string directionAxisX;
    [SerializeField]
    private string directionAxisY;
    [SerializeField]
    private string movementAxisX;
    [SerializeField]
    private string movementAxisY;
    [SerializeField]
    private string rotationAxisLeft;
    [SerializeField]
    private string rotationAxisRight;
    [SerializeField]
    private string useAxis;

    public float AttackAxis { get { return Input.GetAxis(attackAxis); } }

    public float DirectionAxisX { get { return Input.GetAxis(directionAxisX); } }

    public float DirectionAxisY { get { return Input.GetAxis(directionAxisY); } }

    public float MovementAxisX { get { return Input.GetAxis(movementAxisX); } }

    public float MovementAxisY { get { return Input.GetAxis(movementAxisY); } }

    public float RotationAxisLeft { get { return Input.GetAxis(rotationAxisLeft); } }

    public float RotationAxisRight { get { return Input.GetAxis(rotationAxisRight); } }

    public float UseAxis { get { return Input.GetAxis(useAxis); } }
}