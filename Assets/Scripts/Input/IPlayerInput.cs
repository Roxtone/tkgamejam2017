﻿public interface IPlayerInput
{
    float AttackAxis { get; }
    float DirectionAxisX { get; }
    float DirectionAxisY { get; }
    float MovementAxisX { get; }
    float MovementAxisY { get; }
    float RotationAxisLeft { get; }
    float RotationAxisRight { get; }
    float UseAxis { get; }
}